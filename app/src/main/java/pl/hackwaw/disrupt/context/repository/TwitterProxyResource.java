package pl.hackwaw.disrupt.context.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.hackwaw.disrupt.context.domain.ProxyTwitterTweet;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class TwitterProxyResource {

    RestTemplate restTemplate = new RestTemplate();

    public List<ProxyTwitterTweet> since(LocalDateTime from) {
        log.info("Fetching tweets since '{}'", from);
        URI uri = UriComponentsBuilder.fromUriString("http://twitterproxy:8080")
                .path("tweets")
                .queryParam("from", from.toString())
                .queryParam("to", LocalDateTime.now(ZoneOffset.UTC))
                .build()
                .toUri();

        return Arrays.asList(restTemplate.getForObject(uri, ProxyTwitterTweet[].class));
    }
}